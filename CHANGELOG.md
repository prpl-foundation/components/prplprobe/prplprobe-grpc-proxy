# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.0.8 - 2023-11-02(07:12:12 +0000)

### Other

- - [Eyes'ON][prpl] Document Eyes'ON Prpl APIs

## Release v0.0.7 - 2023-10-17(07:12:08 +0000)

### Other

- - [Prpl] Compile containers with ??? version of LCM SDK for Network X

## Release v0.0.6 - 2023-10-06(11:59:28 +0000)

### Other

- Opensource component

## Release v0.0.5 - 2023-09-19(08:44:51 +0000)

### Other

- - [Eyes'ON][prpl] Add CI tests on new Eyes'ON prpl components
- - [Prpl] Rename all "agent" into "prplprobe agent"

## Release v0.0.4 - 2023-08-28(09:13:26 +0000)

### Other

- - [Eyes'ON][prpl] Enable core dump save

## Release v0.0.3 - 2023-08-25(16:10:49 +0000)

## Release v0.0.2 - 2023-08-25(12:58:33 +0000)

### Other

- - [Eyes'ON][prpl] Add a client lib to have common code and simplify integration of new modules
- - [Eyes'ON][prpl] Make SAH CI work on agent and modules
- - [Prpl] Package for open source delivery

## Release v0.0.1 - 2023-04-07(16:56:27 +0000)

### Other

- - [STEP3][TR-181 Proxy] Implement gRPC <--> AMX API proxy (DM + necessary RPCs)

