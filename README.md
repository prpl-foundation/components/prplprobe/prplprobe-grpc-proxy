# Prplprobe gRPC Proxy

Prplprobe is a monitoring system designed to offer key performance indicators (KPIs) about the system (CPU, Memory, Wi-Fi, etc).

## Table of Contents

[[_TOC_]]

## Introduction

`prplprobe-grpc-proxy` is a service used to forward data model changes to other processes through gRPC and expose gRPC configuration changes on the data model.

### Objects and Parameters

To bind gRPC configuration on data model, we need to provide:
- Object: Data model path on which we want to copy the get function's result
- Get function: gRPC function to be called to get the corresponding configuration and copy it on the data model
- Set function: gRPC function to be called when the data model is modified

```mermaid
sequenceDiagram
    participant Bus
    participant proxy
    participant gRPC server

    proxy ->> Bus: create "Object"
    par Refresh DM
        loop Every <interval> seconds
            proxy ->> gRPC server: call "Get function"
            gRPC server ->> proxy: reply protobuf
            opt message changed
                proxy ->> proxy: convert protobuf message using reflection
                proxy ->> Bus: create subobjects, instances and parameters
                proxy ->> proxy: store message
            end
        end
    and DM change handler
        Bus -->> proxy: DM change event
        proxy ->> proxy: convert DM object using reflection
        proxy ->> proxy: populate protobuf message
        proxy ->> gRPC server: call "Set function"
    end
```

### RPCs

To bind gRPC's RPCs with data model's RPCs, we need to provide:
- Object: Data model path on which we want to add the RPC
- RPC function: gRPC function to be called when using the data model's RPC
- Custom binding: function to be called instead of the generic one if we want to manage an RPC differently

In the follwoing diagram, we suppose there isn't any custom binding.

```mermaid
sequenceDiagram
    participant Bus
    participant proxy
    participant gRPC server

    proxy ->> Bus: create "RPC function" on "Object" with an args parameter
    par RPC called on the DM
        Bus -->> proxy: forward arguments
        proxy ->> proxy: convert arguments to protobuf
        proxy ->> gRPC server: call "RPC function"
        gRPC server ->> proxy: return reply
        proxy ->> proxy: convert reply to string
        proxy ->> Bus: return reply
    end
```

## Installation

### Prerequisites

The follwoing packages are required to build `prplprobe-grpc-proxy`:

- CMake

### Dependencies

`prplprobe-grpc-proxy` relies on the following dependencies:

- Protobuf (https://github.com/protocolbuffers/protobuf)
- gRPC (https://github.com/grpc/grpc)
- libsahtrace (https://gitlab.com/prpl-foundation/components/core/libraries/libsahtrace)
- libprplprobe (https://gitlab.com/prpl-foundation/components/prplprobe/libprplprobe)
- libamxrt (https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxrt)

### Build from source

1. **Clone the repository**:

```sh
git clone https://gitlab.com/prpl-foundation/components/prplprobe/prplprobe-grpc-proxy.git
cd prplprobe-grpc-proxy
```

2. **Build the service**

```sh
mkdir build
cd build
cmake ..
make
make install
```

## Documentation

### Prerequisites

The follwoing packages are required to build `prplprobe-grpc-proxy` documentation:

- Doxygen
- Graphviz

### Build doc

Both protobuf and C++ APIs documentations are generated in the same way:

```sh
cmake .. -DGEN_DOC=y
make
make install
```

## Configuration

### Build-time configuration

During the build process, you can customize `prplprobe-grpc-proxy` by setting specific configuration options to meet your project's requirements. Below are the available build configuration options:

- **`GEN_DOC`**: Enabling this option generates documentation for `prplprobe-grpc-proxy`. Make sure to set this to `y` to generate documentation during the build process.

- **`COVERAGE`**: If you wish to include coverage flags and run tests with coverage analysis, enable this option by setting it to `y`.

To configure these options during the build process, you can set them in cmake command directly or use your buildsystem options.

## Run-time Configuration

`prplprobe-grpc-proxy` is configured using a JSON-formatted protobuf message in a configuration file. Below is an example of the configuration file structure:

```json
{
    "sahtrace_config": {
        "sah_trace": {
            "type": "SYSLOG",
            "level": 200
        },
        "trace_zones": [
            {
                "name": "all",
                "level": 200
            }
        ]
    }
}
```

In this example configuration, you can see the structure for specifying sahtrace configuration. Users can modify this JSON file to customize the `prplprobe-grpc-proxy`'s behavior according to their requirements.

## Testing

### Prerequisites

The follwoing packages are required to build `prplprobe-grpc-proxy` documentation:
- gcovr
- valgrind

### Build and run tests

```sh
cmake .. -DCOVERAGE=y
make
```

## License

`prplprobe-grpc-proxy` is licensed under the [BSD-2-Clause-Patent](https://spdx.org/licenses/BSD-2-Clause-Patent.html) license.

You can find a copy of the license in the [LICENSE](./LICENSE) file.

Please review the license carefully before using or contributing to this project. By participating in the `prplprobe-grpc-proxy` community, you agree to adhere to the terms and conditions set forth in this license.

For more details about the license and its implications, refer to the [full license text](https://spdx.org/licenses/BSD-2-Clause-Patent.html).
