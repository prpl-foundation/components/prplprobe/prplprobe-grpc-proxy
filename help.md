à l'init créer un timer (1mn par defaut) qui appelle getBackendsConfiguration --> OK
à chaque appel populer EyesOn.Agent.Backend. avec le retour --> OK
faire un mapping un pour un et transformer le snake case en camel case --> OK

mettre un handler sur l'objet EyesOn.Agent.Backend. --> OK (sur chaque param en fait, read et write)
lorsque EyesOn.Agent.Backend. est modifié faire le parsing inverse --> OK
appeler configureBackendList avec  --> OK


point de vue object :
- object_path: EyesOn.Agent.Backend.
- get_function: getBackendsConfiguration
- set_function: configureBackendList
- get_interval: 60 (seconds)


on pourrait stocker le message protobuf reçu et au get vérifier s'il y a eu des changements avant de tout appliquer dans le DM --> OK
ça peut aussi facilité le set où on aurait juste à changer une valeur dans le message stocké avant de l'envoyer --> OK (en fait on merge dans l'agent au set)
faire attention aux conflits lecture/écriture, bien penser à màj via get avant de set --> OK

binding des RPC
