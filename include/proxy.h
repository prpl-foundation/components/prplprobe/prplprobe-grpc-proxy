/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef GRPC_PROXY_PROXY
#define GRPC_PROXY_PROXY

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_types.h>

#include <prplprobe/amx_client_utils.h>

#ifndef CONFIG_SAH_PRPLPROBE_GRPC_PROXY_PERSIST_PATH
#define CONFIG_SAH_PRPLPROBE_GRPC_PROXY_PERSIST_PATH "/etc/config/prplprobe/collectors/grpc-proxy"
#endif

#define PERSIST_FILE CONFIG_SAH_PRPLPROBE_GRPC_PROXY_PERSIST_PATH "/grpc-proxy.cfg"
#define DEFAULT_FILE PERSIST_FILE ".default"


using namespace google::protobuf;

int proxy(void);

/* Proxy */

template<typename T>
class Proxy {
public:
    using GetFunctionType = std::function<Status(AgentManager::Stub *, ClientContext *, Empty&, T *)>;
    using SetFunctionType = std::function<Status(AgentManager::Stub *, ClientContext *, T&, Result *)>;

    Proxy(ProbeGrpcClient *client, string object_path, GetFunctionType get_function, SetFunctionType set_function, int interval);
    ~Proxy();
    void sync_periodic();
    void write_grpc(string dm_path, const amxc_var_t * const args);

private:
    T build_message_from_dm(const string &dm_path, const amxc_var_t * const value, string object_path);
    static vector<string> parse_path(const string& dm_path);
    static string get_relative_key(string instance_path);
    static void populate_message(Message *current, const amxc_var_t * const value, vector<string> names, string object_path);
    static void amx_timer_cbk(amxp_timer_t *timer UNUSED, void *priv);
    static amxd_status_t write_function(amxd_object_t *object,
                                        amxd_param_t *param,
                                        UNUSED amxd_action_t reason,
                                        const amxc_var_t * const args,
                                        UNUSED amxc_var_t * const retval,
                                        void *priv);
    static amxd_status_t read_function(UNUSED amxd_object_t *object,
                                       amxd_param_t *param,
                                       UNUSED amxd_action_t reason,
                                       UNUSED const amxc_var_t * const args,
                                       amxc_var_t * const retval,
                                       void *priv);
    static void add_parameter_datamodel(amxd_object_t *parent, string name, uint32_t type, void *priv);
    void populate_datamodel(const Message& message, amxd_object_t *base_object);

    ProbeGrpcClient *client_;
    amxp_timer_t *timer_;
    string object_path_;
    GetFunctionType get_function_;
    SetFunctionType set_function_;
    int interval_;
    T message_;
    bool lock_write_;
};

/* END */

#endif // GRPC_PROXY_PROXY
