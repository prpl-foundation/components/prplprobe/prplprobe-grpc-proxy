#!/bin/sh

[ -f /etc/environment ] && source /etc/environment

ulimit -c ${ULIMIT_CONFIGURATION:-0}

start() {
    /usr/lib/prplprobe/collectors/prplprobe-grpc-proxy
}

stop() {
    kill $(pidof prplprobe-grpc-proxy)
}

case $1 in
    start|boot)
        start
        ;;
    stop|shutdown)
        stop
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    debuginfo)
        echo "TODO debuginfo prplprobe-grpc-proxy"
        ;;
    log)
        echo "TODO log prplprobe-grpc-proxy"
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|shutdown|restart|debuginfo|log]"
        ;;
esac
