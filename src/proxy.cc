/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <sstream>

#include <google/protobuf/util/message_differencer.h>

#include <debug/sahtrace.h>

#include <amxrt/amxrt.h>

#include <prplprobe/generic_event.pb.h>
#include <prplprobe/module.pb.h>

#include <prplprobe/amx_utils.h>
#include <prplprobe/config_utils.h>
#include <prplprobe/event_loop.h>
#include <prplprobe/func_utils.h>
#include <prplprobe/sahtrace_utils.h>

#include "proxy.h"
#include "utils.h"

using namespace prplprobe::internal::v1;
using namespace kpi::generic::v1;

using google::protobuf::util::JsonStringToMessage;
using google::protobuf::util::MessageDifferencer;
using google::protobuf::util::MessageToJsonString;

#define UNUSED __attribute__((unused))
#define ME "proxy"

typedef void (*proxy_custom_bind_rpc_t) (amxd_function_t **);

ModuleConfiguration proxy_configuration;

/* Proxy */

template<typename T>
Proxy<T>::Proxy(ProbeGrpcClient *client, string object_path, GetFunctionType get_function, SetFunctionType set_function, int interval)
    : client_(client),
    timer_(NULL),
    object_path_(object_path),
    get_function_(get_function),
    set_function_(set_function),
    interval_(interval),
    lock_write_(false) {
    add_datamodel_from_path(object_path_);
    amxp_timer_new(&timer_, amx_timer_cbk, this);
    amxp_timer_set_interval(timer_, interval_ * 1000);
    amxp_timer_start(timer_, 0);
}

template<typename T>
Proxy<T>::~Proxy() {
    amxp_timer_delete(&timer_);
}

template<typename T>
void Proxy<T>::sync_periodic() {
    T message;
    auto stub = client_->getStub();
    ClientContext context;
    Empty request;
    get_function_(&stub, &context, request, &message);
    /* Update DM only if message has changed */
    if (!MessageDifferencer::Equals(message_, message)) {
        amxd_object_t *base_object = amxd_object_findf(amxd_dm_get_root(amxrt_get_dm()), "%s", object_path_.c_str());
        lock_write_ = true;
        populate_datamodel(message, base_object);
        lock_write_ = false;
        message_.CopyFrom(message);
    }
}

template<typename T>
void Proxy<T>::write_grpc(string dm_path, const amxc_var_t * const args) {
    if (lock_write_) {
        return;
    }
    dm_path.replace(0, object_path_.length(), "");
    SAH_TRACEZ_INFO("debug", "%s", dm_path.c_str());
    sync_periodic();
    T message = build_message_from_dm(dm_path, args, object_path_);
    SAH_TRACEZ_INFO("debug", "Message:\n%s", message.DebugString().c_str());
    auto stub = client_->getStub();
    ClientContext context;
    Result reply;
    set_function_(&stub, &context, message, &reply);
}

template<typename T>
T Proxy<T>::build_message_from_dm(const string &dm_path, const amxc_var_t * const value, string object_path) {
    T message;
    Message *current = &message;

    vector<string> names = parse_path(dm_path);
    populate_message(current, value, names, object_path);

    return message;
}

template<typename T>
vector<string> Proxy<T>::parse_path(const string& dm_path) {
    vector<string> names;
    stringstream ss(dm_path);
    string name;
    while (getline(ss, name, '.')) {
        names.push_back(camel_to_snake(name));
    }
    return names;
}

template<typename T>
string Proxy<T>::get_relative_key(string instance_path) {
    amxd_object_t *object = amxd_object_findf(amxd_dm_get_root(amxrt_get_dm()), "%s", instance_path.c_str());
    return amxd_object_get_value(cstring_t, object, "Key", NULL);
}

template<typename T>
void Proxy<T>::populate_message(Message *current, const amxc_var_t * const value, vector<string> names, string object_path) {
    // Traverse the message using the object and parameter names.
    string current_path = object_path;
    for (const auto& name : names) {
        current_path += name + ".";
        SAH_TRACEZ_INFO("debug", "%s", name.c_str());
        const auto *descriptor = current->GetDescriptor();
        const auto *reflection = current->GetReflection();
        const auto *field = descriptor->FindFieldByName(name);
        if ((field != nullptr) && (field->cpp_type() == FieldDescriptor::CPPTYPE_MESSAGE)) {
            current = reflection->MutableMessage(current, field);
        }
        // In this situation, we assume name is not an object nor a parameter so it's an instance
        // Then let's check if the message has a repeated field and create an instance
        else if (field == nullptr) {
            SAH_TRACEZ_INFO("debug", "%s not found", name.c_str());
            for (int i = 0; i < descriptor->field_count(); i++) {
                field = descriptor->field(i);
                if (field->is_repeated()) {
                    SAH_TRACEZ_INFO("debug", "%s is an instance", name.c_str());
                    current = reflection->AddMessage(current, field);
                    reflection = current->GetReflection();
                    descriptor = current->GetDescriptor();
                    // We assume repeated field have a key field to manage repeated fields / instances
                    field = descriptor->FindFieldByName("key");
                    reflection->SetString(current, field, get_relative_key(current_path));
                    break;
                }
            }
        }
        else {
            // Should be the end of names
            break;
        }
        SAH_TRACEZ_INFO("debug", "Current Path: %s", current_path.c_str());
    }
    SAH_TRACEZ_INFO("debug", "type = %s (%d)", amxc_var_type_name_of(value), amxc_var_type_of(value));

    // Set the parameter value.
    const auto *field = current->GetDescriptor()->FindFieldByName(names.back());
    auto *reflection = current->GetReflection();

    switch (field->cpp_type()) {
        case FieldDescriptor::CPPTYPE_INT32:
            reflection->SetInt32(current, field, amxc_var_constcast(int32_t, value));
            break;
        case FieldDescriptor::CPPTYPE_INT64:
            reflection->SetInt64(current, field, amxc_var_constcast(int64_t, value));
            break;
        case FieldDescriptor::CPPTYPE_UINT32:
            reflection->SetUInt32(current, field, amxc_var_constcast(uint32_t, value));
            break;
        case FieldDescriptor::CPPTYPE_UINT64:
            reflection->SetUInt64(current, field, amxc_var_constcast(uint64_t, value));
            break;
        case FieldDescriptor::CPPTYPE_FLOAT:
            reflection->SetFloat(current, field, amxc_var_constcast(double, value));
            break;
        case FieldDescriptor::CPPTYPE_DOUBLE:
            reflection->SetDouble(current, field, amxc_var_constcast(double, value));
            break;
        case FieldDescriptor::CPPTYPE_BOOL:
            reflection->SetBool(current, field, amxc_var_constcast(bool, value));
            break;
        case FieldDescriptor::CPPTYPE_ENUM:
            reflection->SetEnumValue(current, field, amxc_var_constcast(uint8_t, value));
            break;
        case FieldDescriptor::CPPTYPE_STRING:
            reflection->SetString(current, field, amxc_var_constcast(cstring_t, value));
            break;
        default:
            break;
    }

    SAH_TRACEZ_INFO("debug", "Message:\n%s", current->DebugString().c_str());
}

template<typename T>
void Proxy<T>::amx_timer_cbk(amxp_timer_t *timer UNUSED, void *priv) {
    Proxy *proxy_obj = (Proxy *) priv;
    proxy_obj->sync_periodic();
}

template<typename T>
amxd_status_t Proxy<T>::write_function(amxd_object_t *object,
                                              amxd_param_t *param,
                                              UNUSED amxd_action_t reason,
                                              const amxc_var_t * const args,
                                              UNUSED amxc_var_t * const retval,
                                              void *priv) {

    char *path = amxd_object_get_path(object, AMXD_OBJECT_TERMINATE);
    string full_path = path + string(amxd_param_get_name(param));
    Proxy *proxy_obj = (Proxy *) priv;
    amxc_var_convert(&param->value, args, amxc_var_type_of(&param->value));
    proxy_obj->write_grpc(full_path, &param->value);
    free(path);
    return amxd_status_ok;
}

template<typename T>
amxd_status_t Proxy<T>::read_function(UNUSED amxd_object_t *object,
                                             amxd_param_t *param,
                                             UNUSED amxd_action_t reason,
                                             UNUSED const amxc_var_t * const args,
                                             amxc_var_t * const retval,
                                             void *priv) {

    Proxy *proxy_obj = (Proxy *) priv;
    proxy_obj->sync_periodic();
    amxc_var_copy(retval, &param->value);
    return amxd_status_ok;
}

template<typename T>
void Proxy<T>::add_parameter_datamodel(amxd_object_t *parent, string name, uint32_t type, void *priv) {
    amxd_param_t *param = NULL;
    amxd_param_new(&param, name.c_str(), type);
    amxd_param_add_action_cb(param, action_param_read, read_function, priv);
    amxd_param_add_action_cb(param, action_param_write, write_function, priv);
    amxd_object_add_param(parent, param);
}

template<typename T>
void Proxy<T>::populate_datamodel(const Message& message, amxd_object_t *base_object) {
    const Descriptor *descriptor = message.GetDescriptor();
    const Reflection *reflection = message.GetReflection();

    for (int i = 0; i < descriptor->field_count(); i++) {
        const FieldDescriptor *field = descriptor->field(i);
        string name_camel_case = snakeToCamel(field->name());

        if (field->real_containing_oneof()) {
            const FieldDescriptor *oneof_field = reflection->GetOneofFieldDescriptor(message, field->real_containing_oneof());
            if (oneof_field != field) {
                continue;
            }
        }

        switch (field->cpp_type()) {
            case FieldDescriptor::CPPTYPE_INT32:
                add_parameter_datamodel(base_object, name_camel_case, AMXC_VAR_ID_INT32, this);
                amxd_object_set_int32_t(base_object, name_camel_case.c_str(), reflection->GetInt32(message, field));
                break;
            case FieldDescriptor::CPPTYPE_INT64:
                add_parameter_datamodel(base_object, name_camel_case, AMXC_VAR_ID_INT64, this);
                amxd_object_set_int64_t(base_object, name_camel_case.c_str(), reflection->GetInt64(message, field));
                break;
            case FieldDescriptor::CPPTYPE_UINT32:
                add_parameter_datamodel(base_object, name_camel_case, AMXC_VAR_ID_UINT32, this);
                amxd_object_set_uint32_t(base_object, name_camel_case.c_str(), reflection->GetUInt32(message, field));
                break;
            case FieldDescriptor::CPPTYPE_UINT64:
                add_parameter_datamodel(base_object, name_camel_case, AMXC_VAR_ID_UINT64, this);
                amxd_object_set_uint64_t(base_object, name_camel_case.c_str(), reflection->GetUInt64(message, field));
                break;
            case FieldDescriptor::CPPTYPE_FLOAT:
                add_parameter_datamodel(base_object, name_camel_case, AMXC_VAR_ID_DOUBLE, this);
                amxd_object_set_double(base_object, name_camel_case.c_str(), reflection->GetFloat(message, field));
                break;
            case FieldDescriptor::CPPTYPE_DOUBLE:
                add_parameter_datamodel(base_object, name_camel_case, AMXC_VAR_ID_DOUBLE, this);
                amxd_object_set_double(base_object, name_camel_case.c_str(), reflection->GetDouble(message, field));
                break;
            case FieldDescriptor::CPPTYPE_BOOL:
                add_parameter_datamodel(base_object, name_camel_case, AMXC_VAR_ID_BOOL, this);
                amxd_object_set_bool(base_object, name_camel_case.c_str(), reflection->GetBool(message, field));
                break;
            case FieldDescriptor::CPPTYPE_ENUM:
                add_parameter_datamodel(base_object, name_camel_case, AMXC_VAR_ID_UINT8, this);
                amxd_object_set_uint8_t(base_object, name_camel_case.c_str(), reflection->GetEnumValue(message, field));
                break;
            case FieldDescriptor::CPPTYPE_STRING:
                add_parameter_datamodel(base_object, name_camel_case, AMXC_VAR_ID_CSTRING, this);
                amxd_object_set_cstring_t(base_object, name_camel_case.c_str(), reflection->GetString(message, field).c_str());
                break;
            case FieldDescriptor::CPPTYPE_MESSAGE:
                // Recursively print nested messages
                if (field->is_repeated()) {
                    flush_template_object(base_object);
                    for (int j = 0; j < reflection->FieldSize(message, field); j++) {
                        amxd_object_t *instance = NULL;
                        amxd_object_add_instance(&instance, base_object, NULL, 0, NULL);
                        populate_datamodel(reflection->GetRepeatedMessage(message, field, j), instance);
                    }
                }
                else {
                    add_object_datamodel(base_object, name_camel_case.c_str());
                    populate_datamodel(reflection->GetMessage(message, field), amxd_object_get(base_object, name_camel_case.c_str()));
                }
                break;
        }
    }
}

/* END */

/* RPC binding */

template<typename T_in, typename T_out>
class ProxyRPC {
public:
    using FunctionType = std::function<Status(AgentManager::Stub *, ClientContext *, T_in&, T_out *)>;

    ProxyRPC(ProbeGrpcClient *client, string object_path, string rpc_name, FunctionType rpc, proxy_custom_bind_rpc_t custom_bind)
        : client_(client),
        object_path_(object_path),
        rpc_name_(rpc_name),
        rpc_(rpc) {
        bindRPC(custom_bind);
    }

    ~ProxyRPC() {
    }

    amxd_status_t callRPC(T_in request, T_out *reply) {
        auto stub = client_->getStub();
        ClientContext context;

        // Stub RPC
        Status grpc_status = rpc_(&stub, &context, request, reply);
        return grpc_status.ok() ? amxd_status_ok : amxd_status_unknown_error;
    }

    static amxd_status_t rpc_implem(amxd_object_t *object UNUSED,
                                    amxd_function_t *func,
                                    amxc_var_t *args,
                                    amxc_var_t *ret UNUSED) {
        T_in request;

        string json_string = GET_CHAR(args, "args") ? : "";
        auto status = JsonStringToMessage(json_string, &request);
        if (!status.ok()) {
            SAH_TRACEZ_ERROR(ME, "error: %d", status.code());
        }
        SAH_TRACEZ_INFO("debug", "String received:\n%s", json_string.c_str());
        SAH_TRACEZ_INFO("debug", "Message received:\n%s", request.DebugString().c_str());

        T_out reply;
        ProxyRPC *proxy_obj = (ProxyRPC *) func->priv;
        amxd_status_t ret_status = proxy_obj->callRPC(request, &reply);

        string json_string_out;
        status = MessageToJsonString(reply, &json_string_out);
        if (!status.ok()) {
            SAH_TRACE_ERROR("Failed to parse reply");
        }

        amxc_var_set_type(ret, AMXC_VAR_ID_CSTRING);
        amxc_var_set(cstring_t, ret, json_string_out.c_str());

        return ret_status;
    }

    void bindRPC(proxy_custom_bind_rpc_t custom_bind) {
        amxd_object_t *base_object = amxd_object_findf(amxd_dm_get_root(amxrt_get_dm()), "%s", object_path_.c_str());
        amxd_function_t *func = NULL;
        amxd_function_new(&func, rpc_name_.c_str(), AMXC_VAR_ID_NULL, NULL);
        func->priv = this;
        if (!custom_bind) {
            amxd_function_set_impl(func, rpc_implem);
            amxd_function_new_arg(func, "args", AMXC_VAR_ID_CSTRING, NULL);
            amxd_function_arg_set_attr(func, "args", amxd_aattr_in, true);
        }
        else {
            custom_bind(&func);
        }
        amxd_object_add_function(base_object, func);
    }

private:
    ProbeGrpcClient *client_;
    string object_path_;
    string rpc_name_;
    FunctionType rpc_;
};

/* END */

/* Custom binds */

static amxd_status_t log_events_sop(amxd_object_t *object UNUSED,
                                    amxd_function_t *func,
                                    amxc_var_t *args,
                                    amxc_var_t *ret UNUSED) {

    EventList event_list;
    amxc_var_t *args_events = GET_ARG(args, "events");
    amxc_var_for_each(args_event, args_events) {
        Event *event = event_list.add_event();
        fill_timestamp(event);

        string kpiname = GET_CHAR(args_event, "EventKey") ? : "";
        event->set_kpiname(kpiname);

        amxc_var_t *data_map = GET_ARG(args_event, "EventParameters");

        string kpitype = amxc_var_constcast(cstring_t, amxc_var_take_key(data_map, "Type")) ? : "";
        event->set_kpitype(kpitype);

        Any *event_data = event->mutable_event_data();
        GenericEvent generic;
        auto params = generic.mutable_kpi();
        amxc_var_for_each(data_kpi, data_map) {
            string key = amxc_var_key(data_kpi);
            string value = amxc_var_constcast(cstring_t, data_kpi);
            params->insert({key, value});
        }
        event_data->PackFrom(generic);
    }
    SAH_TRACEZ_INFO("debug", "Message built:\n%s", event_list.DebugString().c_str());

    Result reply;
    ProxyRPC<EventList, Result> *proxy_obj = (ProxyRPC<EventList, Result> *)func->priv;
    amxd_status_t ret_status = proxy_obj->callRPC(event_list, &reply);

    return ret_status;
}

void bind_log_event_sop(amxd_function_t **func) {
    amxd_function_set_impl(*func, log_events_sop);
    amxd_function_new_arg(*func, "events", AMXC_VAR_ID_LIST, NULL);
    amxd_function_arg_set_attr(*func, "events", amxd_aattr_in, true);
}

/* END */

/* Init */

void proxy_init() {
    auto client = init_grpc_client();

    int refresh_interval = 300;

    Proxy<BackendsConfiguration> backend(&client, "Probe.Agent.Backend.", &AgentManager::Stub::getBackendsConfiguration, &AgentManager::Stub::configureBackendList, refresh_interval);
    Proxy<CustomerAuthorization> customer(&client, "Probe.Agent.", &AgentManager::Stub::getCustomerAuthorization, &AgentManager::Stub::setCustomerAuthorization, refresh_interval);

    /* Custo */
    ProxyRPC<EventList, Result> log_events_sop(&client, "Probe.Agent.", "logEvents", &AgentManager::Stub::logEvents, bind_log_event_sop);
    /* Generic */
    ProxyRPC<EventList, Result> log_events(&client, "Probe.Agent.", "logEventsGrpc", &AgentManager::Stub::logEvents, NULL);
    ProxyRPC<BackendsConfiguration, Result> set_backends(&client, "Probe.Agent.", "configureBackendList", &AgentManager::Stub::configureBackendList, NULL);
    ProxyRPC<BackendConfiguration, Result> set_backend(&client, "Probe.Agent.", "configureBackend", &AgentManager::Stub::configureBackend, NULL);
    ProxyRPC<Empty, BackendsConfiguration> get_backends(&client, "Probe.Agent.", "getBackendsConfiguration", &AgentManager::Stub::getBackendsConfiguration, NULL);
    ProxyRPC<BackendName, BackendConfiguration> get_backend(&client, "Probe.Agent.", "getBackendConfiguration", &AgentManager::Stub::getBackendConfiguration, NULL);
    ProxyRPC<CustomerAuthorization, Result> set_customer(&client, "Probe.Agent.", "setCustomerAuthorization", &AgentManager::Stub::setCustomerAuthorization, NULL);
    ProxyRPC<Empty, CustomerAuthorization> get_customer(&client, "Probe.Agent.", "getCustomerAuthorization", &AgentManager::Stub::getCustomerAuthorization, NULL);
    ProxyRPC<QueueingsConfiguration, Result> set_queuing(&client, "Probe.Agent.", "configureQueueing", &AgentManager::Stub::configureQueueing, NULL);
    ProxyRPC<QueueingConfiguration, Result> set_queuings(&client, "Probe.Agent.", "configureQueueingList", &AgentManager::Stub::configureQueueingList, NULL);
    ProxyRPC<Empty, QueueingsConfiguration> get_queuings(&client, "Probe.Agent.", "getQueueingsConfiguration", &AgentManager::Stub::getQueueingsConfiguration, NULL);
    ProxyRPC<Empty, QueueingConfiguration> get_queuing(&client, "Probe.Agent.", "getQueueingConfiguration", &AgentManager::Stub::getQueueingConfiguration, NULL);
    ProxyRPC<Empty, Result> trigger_sending(&client, "Probe.Agent.", "triggerSending", &AgentManager::Stub::triggerSending, NULL);
    ProxyRPC<Empty, prplprobe::internal::v1::Version> get_version(&client, "Probe.Agent.", "getVersion", &AgentManager::Stub::getVersion, NULL);
    ProxyRPC<TimeSlot, Volumetrics> get_volumetrics(&client, "Probe.Agent.", "getVolumetrics", &AgentManager::Stub::getVolumetrics, NULL);
    ProxyRPC<SahTraceConfiguration, Result> set_log_level(&client, "Probe.Agent.", "setLogLevel", &AgentManager::Stub::setLogLevel, NULL);

    amxrt_el_start();
}

int proxy() {
    RestoreConfiguration(proxy_configuration, DEFAULT_FILE, PERSIST_FILE);
    InitSahTrace(proxy_configuration.sahtrace_config());
    init_amx("grpc-proxy");
    add_root_datamodel();
    proxy_init();

    /* Cleanup */
    CleanupSahTrace();
    BackupConfiguration(proxy_configuration, PERSIST_FILE);
    amxrt_stop();
    amxrt_delete();

    return 0;
}

/* END */