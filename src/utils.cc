/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/*
    All stuff to connect with AMX bus
 */
#include <sstream>

#include <debug/sahtrace.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxb/amxb_register.h>
#include <amxb/amxb.h>
#include <amxrt/amxrt.h>

#include "utils.h"

#define ME "utils"

string snakeToCamel(const string &input) {
    istringstream iss(input);
    string result, word;
    while (getline(iss, word, '_')) {
        result += static_cast<char>(word[0] - 32) + word.substr(1);
    }
    return result;
}

string camel_to_snake(const std::string& camel_str) {
    std::string snake_str;
    for (size_t i = 0; i < camel_str.length(); i++) {
        char c = camel_str[i];
        if (std::isupper(c) && i > 0) {
            snake_str += '_';
        }
        snake_str += std::tolower(c);
    }
    return snake_str;
}

void flush_template_object(amxd_object_t *object) {
    object->type = amxd_object_template;
    amxd_object_for_each(instance, it, object) {
        amxd_object_t *inst_obj = amxc_container_of(it, amxd_object_t, it);
        amxd_object_delete(&inst_obj);
    }
}

void add_datamodel_from_path(const string &dm_path) {
    stringstream ss(dm_path);
    string item;
    amxd_object_t *parent = amxd_dm_get_root(amxrt_get_dm());
    while (getline(ss, item, '.')) {
        add_object_datamodel(parent, item);
        parent = amxd_object_get(parent, item.c_str());
    }
}

void add_object_datamodel(amxd_object_t *parent, string name) {
    amxd_object_t *obj = NULL;
    amxd_object_new(&obj, amxd_object_singleton, name.c_str());
    amxd_object_add_object(parent, obj);
}

void add_root_datamodel() {
    amxd_dm_t *dm = amxrt_get_dm();
    add_object_datamodel(amxd_dm_get_root(dm), "Probe");
}
