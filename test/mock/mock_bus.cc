/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "dummy_be.h"
#include "mock_bus.h"

static amxb_bus_ctx_t *bus_ctx = NULL;

amxb_bus_ctx_t* mock_get_bus_ctx() {
    return bus_ctx;
}

void handle_events(void) {
    while (amxp_signal_read() == 0) {
    }
}

void setup_amx_bus() {
    amxd_dm_t *dm = amxrt_get_dm();
    amxo_parser_t *parser = amxrt_get_parser();

    amxd_dm_init(dm);
    amxo_parser_init(parser);

    test_register_dummy_be();
    amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock");
    amxo_connection_add(parser, amxb_get_fd(bus_ctx), connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);

    handle_events();

    amxb_register(bus_ctx, dm);
}

void cleanup_amx_bus() {
    handle_events();
    amxb_free(&bus_ctx);

    amxd_dm_t *dm = amxrt_get_dm();
    amxo_parser_t *parser = amxrt_get_parser();

    amxo_parser_clean(parser);
    amxd_dm_clean(dm);

    test_unregister_dummy_be();

    amxo_resolver_import_close_all();
}

void capture_sigalrm(void) {
    sigset_t mask;

    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM);

    sigprocmask(SIG_BLOCK, &mask, NULL);

    signalfd(-1, &mask, 0);
}

void wait_for_events(uint32_t timeout) {
    sigset_t mask;
    int sfd;
    struct signalfd_siginfo fdsi;
    fd_set set;
    struct timeval ts;

    ts.tv_sec = timeout;
    ts.tv_usec = 0;

    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM);

    sigprocmask(SIG_BLOCK, &mask, NULL);

    sfd = signalfd(-1, &mask, 0);

    FD_ZERO(&set);
    FD_SET(sfd, &set);

    while(select(sfd + 1, &set, NULL, NULL, &ts) > 0) {
        read(sfd, &fdsi, sizeof(struct signalfd_siginfo));

        if(fdsi.ssi_signo == SIGALRM) {
            amxp_timers_calculate();
            amxp_timers_check();
        }
    }
    handle_events();
}

amxd_object_t *mock_add_object_datamodel(amxd_object_t *parent, string name) {
    amxd_object_t *obj = NULL;
    amxd_object_new(&obj, amxd_object_singleton, name.c_str());
    amxd_object_add_object(parent, obj);
    return obj;
}

amxd_object_t *mock_add_template_datamodel(amxd_object_t *parent, string name) {
    amxd_object_t *obj = NULL;
    amxd_object_new(&obj, amxd_object_template, name.c_str());
    amxd_object_add_object(parent, obj);
    return obj;
}

amxd_object_t *mock_add_instance_datamodel(amxd_object_t *parent, string name, uint32_t index) {
    amxd_object_t *obj = NULL;
    amxd_object_add_instance(&obj, parent, name.c_str(), index, NULL);
    return obj;
}

void mock_add_parameter_datamodel(amxd_object_t *parent, string name, uint32_t type) {
    amxd_param_t *param = NULL;
    amxd_param_new(&param, name.c_str(), type);
    amxd_object_add_param(parent, param);
}

